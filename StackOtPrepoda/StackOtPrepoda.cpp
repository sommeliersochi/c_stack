﻿// StackOtPrepoda.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using namespace std;

template<typename T>
class Stack
{
private:
	int NumArray = 0;
	T* StackArray = new T[NumArray];

public:
	Stack()
	{

	}
	//Добавляет элемент
	void Push(T Value)
	{
		T* NewArray = new T[NumArray + 1];
		cout << "Добавлен элемент " << Value << endl;

		for (int i = 0; i < NumArray; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}
	//Удаляет последний элемент
	void Del()
	{
		cout << "Удалён последний элемент " << endl;
		T* NewArray = new T[NumArray - 1];
		for (int i = 0; i < NumArray - 1; i++)
		{
			NewArray[i] = StackArray[i];;
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

	//Достать верхний элемент из стека
	T Pop()
	{
		return StackArray[NumArray - 1];
	}

	//Печатает массив
	void Print()
	{
		for (int i = 0; i < NumArray; i++)
		{

			cout << *(StackArray + i) << "   ";
		}

		cout << endl;
	}
	//Чистим память
	~Stack()
	{
		delete[] StackArray;
	}

};

int main()



{
	setlocale(LC_ALL, "ru");

	Stack<int> st;

	st.Push(10);	
	cout << st.Pop() << endl;
	st.Push(100);
	st.Push(111);
	st.Print();
	st.Del();
	st.Print();

	Stack<string> ss;
	ss.Push("a");
	ss.Push("b");
	ss.Push("c");
	ss.Print();
	ss.Del();
	ss.Push("d");
	ss.Print();



}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
